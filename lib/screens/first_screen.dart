import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sec5/screens/meal_screen.dart';
import 'package:flutter_sec5/widgets/fus.dart';

class FirstScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color(0xfffffee6),
      appBar: AppBar(
        backgroundColor: Color(0xff8cc34a),
        title: Text("Quick & Easy"),
      ),
      body: ListView.builder(
          itemCount: data.length,
          itemBuilder:((context ,index){
        return Padding(
          padding: const EdgeInsets.all(10.0),
          child: InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return Meal();
              }
              ),
                //(Route<dynamic>route)=>false
              );
            },
            child: Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Column(
                children: [
                  Stack(
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0),topRight:Radius.circular(20.0)),
                          child: Image.network(data[index = index].url)),
                      Positioned(
                        bottom: 20,
                        right: 15,
                        child: Text("pizza",
                          style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30,backgroundColor: Colors.black45,
                          color: Colors.white),),
                      ),
                    ],
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(vertical:12.0 ,horizontal:12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Row(
                          children: [
                            Icon(Icons.access_time_rounded),
                            SizedBox(width: 5,),
                            Text(data[index].title,style: TextStyle(fontSize: 15,
                                fontWeight: FontWeight.w400),),
                          ],
                        ),
                        Row(
                          children: [
                            Icon(Icons.shopping_basket),
                            SizedBox(width: 5,),
                            Text(data[index].desc,style: TextStyle(fontSize: 15,
                                fontWeight: FontWeight.w400)),
                          ],
                        ),
                        Row(
                          children: [
                            Icon(Icons.monetization_on_outlined),
                            SizedBox(width: 5,),
                            Text(data[index].price,style: TextStyle(fontSize: 15,
                                fontWeight: FontWeight.w400)),
                          ],
                        ),

                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      }))

    );
  }
}
