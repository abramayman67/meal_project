import 'package:flutter/material.dart';
import 'package:flutter_sec5/screens/first_screen.dart';
import 'package:flutter_sec5/widgets/fus.dart';

import 'filter_screen.dart';

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Color(0xfffffee6),
      drawer: Drawer(
        child: Container(
          color:  Color(0xffffffe7),
          child: Column(
            children: [
              Container(
                color: Color(0xfffcc108),
                width: double.infinity,
                height: 100,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical:30.0,horizontal: 15.0),
                  child: Text("Cooking Up!", style: TextStyle(color: Color(0xffadb929),
                      fontSize: 25,fontWeight: FontWeight.bold),),
                ),
              ),

              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return SecondScreen();
                  }
                  ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Icon(Icons.no_meals,size: 23,color: Colors.grey,),
                      SizedBox(
                        width: 20,
                      ),
                      Text("Meals" ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return FilterScreen();
                  }
                  ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Icon(Icons.settings,size: 23,color: Colors.grey,),
                      SizedBox(
                        width: 20,
                      ),
                      Text("Filters" ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      appBar: AppBar(
        backgroundColor: Color(0xff8cc34a),
        title: Text("Categories"),
      ),
      body: GridView.count(
        primary: true,
        padding: const EdgeInsets.all(15.0),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        shrinkWrap: true,
        children: <Widget>[
          InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return FirstScreen();
                }
                ),
                );
              },
              child:seconditem("Italian", Colors.deepPurpleAccent)),
          seconditem("Quick & Easy", Colors.redAccent),
          seconditem("Hamburgers", Colors.amber),
          seconditem("German", Colors.amberAccent),
          seconditem("Light & Lovely", Colors.blue),
          seconditem("Exotic", Colors.green),
          seconditem("Breakfast", Colors.lightBlueAccent),
          seconditem("Asian", Colors.lightGreenAccent),
          seconditem("French", Colors.pinkAccent),
          seconditem("Summer", Colors.blueGrey),


        ],
      ),
    );
  }
}
