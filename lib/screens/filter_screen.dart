import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sec5/widgets/fus.dart';

class FilterScreen extends StatefulWidget {

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Color(0xfffffee6),
      appBar: AppBar(
        backgroundColor: Color(0xff8cc34a),
        title: Text(
          "My Filters"
        ),
      ),
      body: Column(
        children: [
          SizedBox(height: 10,),
          Text("Adjust Your Meal Selection", style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
          SizedBox(height: 18,),
          filteritem(),
          filteritem(),
          filteritem(),
          filteritem(),
        ],
      ),
    );
  }

  bool switchValue=false;

  Widget filteritem(){
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Gluten",style:TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
              Text("Only Inxlude Gluten Meals",style:TextStyle(fontSize: 10,fontWeight: FontWeight.bold),)

            ],
          ),
          CupertinoSwitch(
            value: switchValue,
            onChanged: (newvalue){
              setState((){
                switchValue= newvalue;
              });
            },
          ),
        ],
      ),
    );
  }
}
