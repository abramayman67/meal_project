import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sec5/screens/first_screen.dart';

class items {
  String title;
  String desc;
  String price;
  String url;
  String name;

  items({this.title, this.desc, this.price,this.url,this.name});
}
List <items> data=[items(title:"20 min", desc: "orange color", price: "20", url: "https://tse1.mm.bing.net/th?id=OIP.ydGt59tsxF9_Yo7S79epUgHaHa&pid=Api&P=0", name:"pizza" ),
  items(title:"15 min", desc: "red color", price: "40", url: "https://tse3.mm.bing.net/th?id=OIP.uPJcJpbeVGUkSSOSJdQKcQHaE8&pid=Api&P=0",name: "passta" ),
  items(title:"35 min", desc: "yellow color", price: "20", url: "https://tse2.mm.bing.net/th?id=OIP.dK2zXqb5LM1IN7a_JM8v6wHaE8&pid=Api&P=0",name: "burger" ),
  items(title:"10 min", desc: "with nuts", price: "20", url: "https://tse3.mm.bing.net/th?id=OIP.lLYnf87IJXPZqFVYyyCZDgHaDt&pid=Api&P=0", )
];

Widget seconditem(String text, Color color ){
  return Container(
    height: 20,
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0),color: color,),
    padding:  EdgeInsets.all(8),
    child:  Text("${text}" ,style:  TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
  );
}